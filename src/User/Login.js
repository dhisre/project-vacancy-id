import React, { useState } from "react";
import axios from "axios";
import { useNavigate } from "react-router";
import Cookies from "js-cookie";
import Navbar from "../Home/navbar";
import Footer from "../Home/footer";

const Login = () => {

  const styles = {
    
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column',
    height: '100vh',
    width: '50%',
    // backgroundColor: 'yellow'
    
  };

  const [fetchStatus, setFetchStatus] = useState(true)
  let navigate = useNavigate();
  const [input, setInput] = useState(
    {
    
    email : "",
    password : ""

    }, [fetchStatus, setFetchStatus])

const handleChange = (event) => {
    console.log("event", event)
    let value = event.target.value
    let name = event.target.name

    setInput({...input, [name] : value})
}

const handleLogin = (event) => { 
  
    event.preventDefault()
      let {
        email, password
        } = input 
        console.log("email", email)
        console.log("pasword", password)
       const data = {
       
          email:email,
          password:password,

        }
        
    axios.post('https://dev-example.sanbercloud.com/api/login'  , data)
      .then((res) => {
        console.log(res)
        setFetchStatus(true)
        let {token} = res.data
        Cookies.set('token', token)
        navigate("/profile", { replace: true });
        
      }
      )
      }
  


    return(
      <>
      <Navbar />
      <div>
      <form style={styles} onSubmit={handleLogin} className="container mx-auto mt-10">
      <h5 className="text-5xl font-medium text-gray-900 dark:text-white">Sign in to VacancyID</h5>
      
      <div className="mb-6 mt-10">
         <label htmlFor="email" className="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Your email</label>
        <input onChange={handleChange} value={input.email} type={'text'} name="email" className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="email" required />
      </div>
      <div className="mb-6">
        <label htmlFor="password" className="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Your password</label>
        <input onChange={handleChange} value={input.password} type='password' name="password" className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" required />
      </div>
      <button onChange={handleLogin} type="submit" className="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">Login</button>
    </form>
    </div>

    <Footer />
    </>
    )

}
export default Login