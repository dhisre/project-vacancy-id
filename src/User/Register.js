import React, { useState } from "react";
import axios from "axios";
import { useNavigate } from "react-router";
import Navbar from "../Home/navbar";
import Footer from "../Home/footer";

const Register = () => {

  const styles = {
    
    alignItems: 'center',
    justifyContent: 'center',
    flexDirection: 'column',
    height: '100vh',
    width: '50%',
    
  };

  const [fetchStatus, setFetchStatus] = useState(true)
  let navigate = useNavigate();
  const [input, setInput] = useState(
    {
    name: "",
    email : "",
    password : ""

    }, [fetchStatus, setFetchStatus])

const handleChange = (event) => {
    console.log("event", event)
    let value = event.target.value
    let name = event.target.name

    setInput({...input, [name] : value})
}

const handleSubmit = (event) => { 
  
    event.preventDefault()
    


      let {
        name, email, password
        } = input 
        console.log("email", email)
        console.log("pasword", password)
        console.log("name", name) 
        const imageProfile = "https://images.unsplash.com/photo-1535713875002-d1d0cf377fde?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=580&q=80"
        const data = {
          name:name, 
          image_url: imageProfile, 
          email:email,
          password:password,

        }
        
    axios.post('https://dev-example.sanbercloud.com/api/register'  , data)
      .then((res) => {
        console.log(res)
        setFetchStatus(true)
        navigate("/login", { replace: true });
        
      }
      )
      }
  


    return(
      <>
      <Navbar />
      <div>
      <form style={styles} onSubmit={handleSubmit} className="container mx-auto mt-10">
      <div className="mb-6">
         <label htmlFor="name" className="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Your name</label>
        <input onChange={handleChange} value={input.name} type={'text'} name="name" className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="Your Name" required />
      </div>
      <div className="mb-6">
         <label htmlFor="email" className="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Your email</label>
        <input onChange={handleChange} value={input.email} type={'text'} name="email" className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="Email" required />
      </div>
      <div className="mb-6">
        <label htmlFor="password" className="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Your password</label>
        <input onChange={handleChange} value={input.password} type='password' name="password" className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" required />
      </div>
      <button type="submit" className="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">Register</button>
    </form>
    </div>
    <Footer />
    </>
    )

}
export default Register