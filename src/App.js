import { GlobalContext, GlobalProvider } from './context/GlobalContext';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import Home from './Home/Home';
import CompanyProfile from './Profile/CompanyProfile';
import Register from './User/Register';
import Vacancy from './JobVacancy/Vacancy';
import Login from './User/Login';
import Profile from './User/Profile';
const App = () => {
  return (
    
    <>


    
 <BrowserRouter>
        
    <Routes>

      <Route path='/' element={<Home />} />
      <Route path='/home/company-profile' element={<CompanyProfile />} />
      <Route path='/register' element={<Register />} />
      <Route path='/vacancy' element={<Vacancy />} />
      <Route path='/login' element={<Login />} />
      <Route path='/profile' element={<Profile />} />


    </Routes>
      
  </BrowserRouter>
    


    </>
  );
}

export default App;
