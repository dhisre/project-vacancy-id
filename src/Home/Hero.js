import { data } from "autoprefixer";
import React, { useState } from "react";
import background from "./Hero1.jpg";


const Hero = () => {
  const [searchTerm, setSearchTerm] = useState("");

  const styles = {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    height: '100vh',
  };
  return (
    <div className="hero min-h-screen" style={{backgroundImage: `url(${background})`, minWidth: '300px', minHeight: '300px'}} >
       
      <div className="hero-overlay bg-opacity-60"  />
      <div className="hero-content text-center text-neutral-content" >  
      <div style={styles}>   
        <div className="max-w-2xl">
          
          <h1 className="text-6xl font-semibold whitespace-nowrap text-white bg-dark mb-5 ">Welcome to VacancyID</h1>
          <p className="text-3xl mb-5 text-white bg-dark"></p>
          <a href="/vacancy" className="text-2xl btn btn-primary text-white bg-dark" >Find Vacancy</a> 
        
   
         
        </div>
      </div>
    </div>
    </div>
  );
}
export default Hero