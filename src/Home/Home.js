import React from "react";
import Navbar from "./navbar";
import Footer from "./footer";
import Hero from "./Hero";



const Home = () => {
    return (

        <>
        
        <Navbar />
        <Hero />
        <Footer />

        </>

    )

}
export default Home