import React, {useEffect, useState} from "react";
import axios from 'axios';
import Navbar from "../Home/navbar";

const Vacancy = () => {

    const [data, setData] = useState([])
    useEffect(() => {

        
        axios.get("https://dev-example.sanbercloud.com/api/job-vacancy")
        .then((res) => {
            console.log("resdata", res)
            setData(res.data.data) 
            
        })
        .catch((error) => {
        })
    
    
    }, [])
console.log("datavacancy", data)    

const handleDelete = (event) => {
  

let idData = parseInt (event.target.value)

    axios.delete(`https://dev-example.sanbercloud.com/api/job-vacancy/${idData}`)
    .then((del) => {
      console.log("delete", del)

    })
    .catch((error) => {

    })
  
}

    return (
    <>
    <Navbar />

    <div className="bg-gray-100">
      <div className="mx-auto max-w-7xl px-4 sm:px-6 lg:px-8">
        <div className="mx-auto max-w-2xl py-16 sm:py-24 lg:max-w-none lg:py-32">
          <h2 className="text-4xl font-bold text-gray-900 text-center">Vacancy List</h2>

          <div className="mt-6 space-y-12 lg:grid lg:grid-cols-3 lg:gap-x-6 lg:space-y-0">
            {data.map((res, index) => (
              <div key={res.company_name} className="group relative">
                 <h2 className="mt-6 text-3xl text-gray-900 text-center py-2">
                  {res.company_name}
                </h2>
                <div className="relative h-80 w-full overflow-hidden rounded-lg bg-white hover:opacity-75 sm:aspect-w-2 sm:aspect-h-1 sm:h-64 lg:aspect-w-1 lg:aspect-h-1">
                  <img
                    src={res.company_image_url}
                    alt="#"
                    className="h-full w-full object-cover object-center"
                  />
                </div>
                  <div className="mt-6 p-6 max-w-lg bg-white rounded-lg border border-gray-200 shadow-md dark:bg-gray-800 dark:border-gray-700">
                <p className="text-base font-semibold text-gray-900 py-3">Job Description: {res.job_description}</p>
                <p className="text-base font-semibold text-gray-900 py-3">Job Qualification: {res.job_qualification}</p>
                <p className="text-base font-semibold text-gray-900 py-3">Office Location: {res.job_company_city}</p>
                <p className="text-base font-semibold text-gray-900 py-3">Tenure: {res.job_tenure}</p>
                <p className="text-base font-semibold text-gray-900 py-3">Job Type: {res.job_type}</p>
                <p className="text-base font-semibold text-gray-900 py-3">Title: {res.job_title}</p>
                <p className="text-base font-semibold text-gray-900 py-3">Salary: Rp.{res.salary_min} - Rp.{res.salary_max}</p>
                    
                    <div className="relative h-16 w-88">
                <button className="bg-slate-400 text-white w-16 h-16 absolute bottom-0 left-0 rounded-lg hover:opacity-75 ">Edit</button>
                <button className="bg-slate-400 text-white w-16 h-16 absolute bottom-0 right-0 rounded-lg hover:opacity-75 " onClick={handleDelete} value={res.id}>Delete</button>
                    </div>
                  </div>
              </div>
            ))}
          </div>
        </div>
      </div>
    </div>

    </>
    )   
}
export default Vacancy
