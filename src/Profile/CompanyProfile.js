import React from "react";

const CompanyProfile = () => {
    return (

        <div className="hero min-h-screen" style={{backgroundImage: 'url(https://images.unsplash.com/photo-1533090161767-e6ffed986c88?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1169&q=80)'}}>
          <div className="hero-overlay bg-opacity-60" />
          <div className="hero-content text-center text-neutral-content">
            <div className="max-w-md">
              <h1 className="self-center text-2xl font-semibold whitespace-nowrap dark:text-white">Hello there</h1>
              <p className="mb-5"> Detail Section </p>
              <button className="btn btn-primary">Find Vacancy</button>
            </div>
          </div>
        </div>
      );
}
export default CompanyProfile